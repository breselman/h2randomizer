"use strict";

var firstNames = require("../data/first_names");
var lastNames = require("../data/last_names");
var randomizer = require("../index");
var assert = require('chai').assert;
var expect = require('chai').expect;


describe("randomizer works with loading from requiress", function () {
  this.timeout(5000);
  it("has no blank first names", function (done) {
    firstNames.forEach(function (item) {
      expect(item).to.be.a("string");
      assert.isTrue(item.length > 0)
    })
    done();
  });

  it("has no blank last names", function (done) {
    lastNames.forEach(function (item) {
      expect(item).to.be.a("string");
      assert.isTrue(item.length > 0)
    })
    done();
  });

  it("can get first name", function (done) {
    var str = randomizer.getRandomFirstNameSync();
    console.log("produces [ " + str + " ]\n");
    expect(str).to.be.a("string");
    done();

  });

  it("can get random phone number usa", function (done) {
    var str = randomizer.getRandomPhoneNumberUsaSync();
    console.log("produces [ " + str + " ]\n");
    expect(str).to.be.a("string");
    assert.isTrue(str.length == 10);
    done();
  });

  it("can get last name", function (done) {
    var str = randomizer.getRandomLastNameSync();
    console.log("produces [ " + str + " ]\n");
    expect(str).to.be.a("string");
    done();
  });

  it("can get a City", function (done) {
    var o = randomizer.getRandomCitySync();
    console.log("produces [ " + JSON.stringify(o) + " ]\n");
    expect(o).to.be.an("object");
    done();
  });

  it("can a gender", function (done) {
    var diff = false;
    var lastVal;

    for (var i = 0; i < 10; i++) {
      var o = randomizer.getRandomGenderSync();
      if (lastVal && lastVal != o) {
        diff = true;
      }

      expect(o).to.be.an("string");
      console.log("produces [ " + o + " ]\n");
      lastVal = o;
    }
    assert.isTrue(diff);
    done();
  });

  it("can get an Email Domain", function (done) {
    var str = randomizer.getRandomEmailDomainSync();
    console.log("produces [ " + str + " ]\n");
    expect(str).to.be.an("string");
    done();
  });

  it("can get a Address One", function (done) {
    var str = randomizer.getRandomAddressOneSync();
    console.log("produces [ " + str + " ]\n");
    expect(str).to.be.an("string");
    done();
  });

  it("can get a Address Two", function (done) {
    var str = randomizer.getRandomAddressTwoSync();
    console.log("produces [ " + str + " ]\n");
    expect(str).to.be.an("string");
    done();
  });

  it("can get a Password", function (done) {
    var str = randomizer.getRandomPasswordSync();
    console.log("produces [ " + str + " ]\n");
    expect(str).to.be.an("string");
    done();
  });

  it("can get a random boolean", function (done) {
    var b = randomizer.getRandomBooleanSync();
    console.log("produces [ " + b + " ]\n");
    expect(b).to.be.a("boolean");
    done();
  });

  it("can get a Date", function (done) {
    var d = randomizer.getRandomDateSync(new Date("11/12/1998"), new Date());
    console.log("produces [ " + d + " ]\n");
    expect(d).to.be.a("date");
    done();
  });

  it("can get a Callback URL", function (done) {
    var str = randomizer.getRandomCallbackUrlSync();
    console.log("produces [ " + str + " ]\n");
    expect(str).to.be.a("string");
    done();
  });

  it("can get Random description", function (done) {
    var str = randomizer.getRandomDescriptionSync();
    assert.isTrue(str.split(" ").length == 10);
    console.log("produces [ " + str + " ]\n");
    done();
  });

  it("can get random number within the max and  min", function (done) {

    for (var t = 0; t < 10000; t++) {
      var min;
      while (!min || min == 0) {
        min = Math.floor(Math.random() * 100);
      }
      var max = 0;
      while (max <= min) {
        max = min + Math.floor(Math.random() * 100);
      }
      var num = randomizer.getRandomIntInclusiveSync(min, max);
      assert.isTrue(num <= max);
      assert.isTrue(min <= num);
    }
    done();
  });

  it("cannot get random number with no  max and  min", function (done) {

    for (var t = 0; t < 10; t++) {
      try {
        randomizer.getRandomIntInclusiveSync();
      } catch (e) {
        assert.isFalse(!e)

      }
    }
    done();
  });

  it("cannot get random number with no  max", function (done) {

    for (var t = 0; t < 10; t++) {
      try {
        randomizer.getRandomIntInclusiveSync(Math.floor(Math.random() * 100));
      } catch (e) {
        assert.isFalse(!e)

      }
    }
    done();
  });


  it("cannot get random number when min and max are equal", function (done) {
    for (var t = 0; t < 10; t++) {
      try {
        var j = Math.floor(Math.random() * 100);
        randomizer.getRandomIntInclusiveSync(j, j);
      } catch (e) {
        assert.isFalse(!e)
      }
    }
    done();
  });
  it("cannot get random number when max is less than min", function (done) {

    for (var t = 0; t < 10; t++) {
      try {
        var i = Math.floor(Math.random() * 100);
        var j = 0;
        while (j == 0) {
          j = Math.floor(Math.random() * 100);
        }
        j = i - j;

        randomizer.getRandomIntInclusiveSync(i, j);
      } catch (e) {
        assert.isFalse(!e)
      }
    }
    done();
  });

});

