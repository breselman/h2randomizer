"use strict"
var uuid = require('node-uuid');
var firstNames = require("./data/first_names");
var lastNames = require("./data/last_names");
var cities = require("./data/cities");
var words = require("./data/words");

module.exports = {
    getRandomArrayItemSync:rand,
    getRandomBooleanSync:getRandomBooleanSync,
    getRandomFirstNameSync:getRandomFirstName,
    getRandomLastNameSync:getRandomLastName,
    getRandomEmailDomainSync:getRandomEmailDomain,
    getRandomGenderSync:getRandomGenderSync,
    getRandomDateSync:randomDate,
    getRandomPasswordSync:randomPassword,
    getRandomCitySync:getRandomCity,
    getRandomAddressOneSync: getRandomAddressOne,
    getRandomAddressTwoSync: getRandomAddressTwo,
    getRandomCallbackUrlSync:getRandomCallbackUrl,
    getRandomPhoneNumberUsaSync:getRandomPhoneNumberUsa,
    getRandomDescriptionSync:getRandomDescription,
    getRandomIntInclusiveSync:getRandomIntInclusive,
    rand:rand
};

/**
 * @return {String} a Guid
 */
function randomPassword(){
    return uuid.v4();
}

/**
 * Adds two numbers
 * @param {Number} min The minimum random value to return, inclusive, must be of a value at least 1
 * @param {Number} max The maximum random value to return, inclusive, must be greater than minimum value
 * @return {Number} a random number between the min and max value inclusive
 */
function getRandomIntInclusive(min, max) {
    if(!min && !max) throw "No minimum and maximum parameters provided."
    if(!min) throw "No minimum value parameter provided.";
    if(!max) throw "No maximum value parameter provided.";
    if(min === max) throw "Minimum and maximum value are equal: " + min +", " + max ;
    if(min > max) throw "Minimum is greater than maximum value: Min - " + min +" , Max - " + max;
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


function getRandomPhoneNumberUsa(){
    return Math.floor(Math.random() * 10).toString() +
    Math.floor(Math.random() * 10).toString() +
    Math.floor(Math.random() * 10).toString() +
    Math.floor(Math.random() * 10).toString() +
    Math.floor(Math.random() * 10).toString() +
    Math.floor(Math.random() * 10).toString() +
    Math.floor(Math.random() * 10).toString() +
    Math.floor(Math.random() * 10).toString() +
    Math.floor(Math.random() * 10).toString() +
    Math.floor(Math.random() * 10).toString()
}

function getRandomCallbackUrl(){
    return rand(getCallbackUrls());
}

function getRandomAddressOne(){
    var n = Math.floor(Math.random() * 100);
    var address = rand(getStreetNames()) + " " + rand(getStreetTypes());
    return n + " " + address;

}

function getRandomAddressTwo(){
    var n = Math.floor(Math.random() * 100);
    var typ = rand(getAddressTypes());
    return typ + " " + n;

}

function rand(items) {
    return items[~~(Math.random() * items.length)];
}
function getRandomBooleanSync(){
    var n = Math.floor(Math.random() * 100);
    return (n % 2 == 0);
}
function getCallbackUrls(){
    var a = [];
    for(var i = 0;i<30;i++){
        var s = "http://www." + rand(getStreetNames()) + getRandomFirstName() + ".com/services/";
        a.push(s);
    }
    return a;
}

function getRandomFirstName(){
    return rand(firstNames);
}

function getRandomLastName(){
    return rand(lastNames);
}

function getRandomEmailDomain(){
    return rand(getEmailDomains());
}
function getRandomCity(){
    return rand(cities);
}

function getRandomDescription(numberOfWords){
    var num = 0;
    if(!numberOfWords){
        num = 10;
    }else{
        num = numberOfWords
    }
    var str = "";
    for(var i = 0;i<num;i++){
        str = str + rand(words) + " ";
    }
    //chop off the last space.
    return str.substr(0,str.length -1);
}

function getStreetNames() {
    var names = [];
    names.push("Main");
    names.push("Elm");
    names.push("Maple");
    names.push("Oak");
    names.push("Birch");
    names.push("Rose");
    names.push("Sherman");
    names.push("Lincoln");
    names.push("Washington");
    names.push("Hamilton");
    names.push("Madison");
    names.push("Monoe");

    return names;
}

function getStreetTypes() {
    var names = [];
    names.push("St.");
    names.push("Ave.");
    names.push("Dr.");
    names.push("Circle");
    names.push("Highway");
    names.push("Blvd.");
    return names;
}

function getAddressTypes() {
    var names = [];
    names.push("Suite.");
    names.push("Room.");
    names.push("Building");
    names.push("Floor");
    names.push("Mailstop");
    return names;
}


function getEmailDomains() {
    var values = [];
    values.push("git.com");
    values.push("download.com");
    values.push("cnet.com");
    values.push("cheeseburger.com");
    values.push("att.com");
    values.push("gmail.com");
    values.push("yahoo.com");
    values.push("roadrunner.com");
    values.push("msn.com");
    values.push("verizon.com");
    values.push("nbc.com");
    values.push("cbs.com");
    values.push("abc.com");
    values.push("pbs.com");
    values.push("fox.com");
    return values;
}

function getRandomGenderSync() {
    var n = Math.floor(Math.random() * 100);
    if (n % 2 == 0) {
        return "male";
    } else {
        return "female";
    }
}


function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

